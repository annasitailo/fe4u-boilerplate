// import randomUserMock from './FE4U-Lab3-mock'
// const testModules = require('./tesmodulet-');
// console.log(testModules.message);
// require('../css/app.css');

// /** ******** Your code here! *********** */


const {randomUserMock, additionalUsers} = require("./FE4U-Lab3-mock");

function task1(arr1, arr2)
{
    var results = [];
    var favorite = [true, false];
    var courses = ["Mathematics", "Physics", "English", "Computer Science", "Dancing", "Chess", "Biology", "Chemistry", "Law", "Art", "Medicine", "Statistics"];
    var color = ["#E2D4B7", "#9C9583", "#A1A499", "#B0BBBF", "#CADBC8", "#8A716A", "#C2B8B2", "#197BBD", "#125E8A", "#204B57", "#B4B8AB", "#153243", "#284B63", "#F4F9E9", "#EEF0EB"];
    var notes = ["-", "Hi, nice to meet you!"];
    arr1.forEach(element => {
        let obj = {};
        let randId = Math.floor(Math.random() * 1000);
        let randFav = Math.floor(Math.random() * 2);
        let randCourse = Math.floor(Math.random() * 12);
        let randColor = Math.floor(Math.random() * 15);
        let randNote = Math.floor(Math.random() * 2);
        obj.id = Date.now() + randId;
        obj.gender = element.gender;
        obj.title = element.name.title;
        obj.full_name = element.name.first + " " + element.name.last;
        obj.city = element.location.city;
        obj.state = element.location.state;
        obj.country = element.location.country;
        obj.postcode = element.location.postcode;
        obj.coordinates = {};
        obj.coordinates.latitude = element.location.coordinates.latitude;
        obj.coordinates.longitude = element.location.coordinates.longitude;
        obj.timezone = {};
        obj.timezone.offset = element.location.timezone.offset;
        obj.timezone.description = element.location.timezone.description;
        obj.email = element.email;
        obj.b_day = element.dob.date;
        obj.age = element.dob.age;
        obj.phone = element.phone;
        obj.picture_large = element.picture.large;
        obj.picture_thumbnail = element.picture.thumbnail;
        obj.favorite = favorite[randFav];
        obj.course = courses[randCourse];
        obj.bg_color = color[randColor];
        obj.note = notes[randNote];
        results.push(obj);
    });

    for (let i = 0; i < arr2.length; i++)
    {
        var isInArr = false;
        for (let j = 0; j < results.length; j++)
        {
            if (arr2[i].full_name === results[j].full_name)
            {
                isInArr = true;
            }
        }
        if (!isInArr)
        {
            results.push(arr2[i]);
        }
    }
    console.log(results);
    return results;
}

function task2(arr)
{
    var arr1 = ["full_name", "gender", "note", "state", "city", "country"];
    arr.forEach((element, i) => {
        console.log(i);
        arr1.forEach(fieldName => 
            {
                let value = element[fieldName];
                if (value == null || value == " ")
                {
                    console.log(fieldName + ": empty value");
                }
                else if (typeof(value) !== 'string')
                {
                    console.log(fieldName + ": type is not string!");
                }
                else if (value[0] != value[0].toUpperCase())
                {
                    console.log(fieldName + ": first letter must be in upper case!");
                }
            });
        if (typeof(element.age) == null || !Number.isInteger(element.age))
        {
            console.log("age: must be a number!");
        }
        if (element.email == null || typeof(element.email) != 'string' || !String(element.email).includes("@"))
        {
            console.log("email: must be string and include '@'!");
        }
    });
}

function filter(arr, param)
{
    var filteredArr = [];
    arr.forEach(element => {
        var flag = true;
        Object.keys(param).forEach(fieldName => {
            flag = flag && (element[fieldName] == param[fieldName]);
        });
        if (flag)
        {
            filteredArr.push(element);
        }
    });
    return filteredArr;
}

function task3(arr, param) 
{
    var filteredArr = filter(arr, param);
    console.log(filteredArr);
    return filteredArr;
}

function task4(arr, sortingParam)
{
    var sortedArr = [];
    if (["full_name", "age", "country"].indexOf(sortingParam.fieldName) != -1)
    {
        sortedArr = arr.sort((a, b)=> (sortingParam.direction == "asc" ? a[sortingParam.fieldName].localeCompare(b[sortingParam.fieldName]) : b[sortingParam.fieldName].localeCompare(a[sortingParam.fieldName])));
    }
    else if (sortingParam.fieldName == "age")
    {
        sortedArr = arr.sort((a, b)=> (sortingParam.direction == "asc" ? a.age - b.age : b.age - a.age));
    }
    console.log(arr);
    return sortedArr;
}

function task5(arr, param)
{
    var results = filter(arr, param);
    if (results != null && results.length > 0)
    {
        console.log(results[0]);
        return results[0];
    }
    return [];
}

function task6(arr, searchingField, param)
{
    var numberOfResults = 0;
    var percentageOfResults = 0;
    arr.forEach(element => {
        switch(searchingField)
        {
            case "full_name":
                if (element.full_name == param)
                {
                    numberOfResults++;
                }
                break;
            case "age>=":
                if (element.age >= param)
                {
                    numberOfResults++;
                }
                break;
            case "age<":
                if (element.age < param)
                {
                    numberOfResults++;
                }
                break;
            case "gender":
                if (element.gender == param)
                {
                    numberOfResults++;
                }
                break;
            case "country":
                if (element.country == param)
                {
                    numberOfResults++;
                }
                break;
        }
    });
    percentageOfResults = ((numberOfResults / arr.length) * 100).toFixed(1);
    console.log(percentageOfResults);
    return percentageOfResults;
}

var t1 = task1(randomUserMock, additionalUsers);
task2(t1);
task3(t1, {country: "Germany", gender: "female", state: "Bayern"});
task4(t1, {fieldName: "country", direction: "asc"});
task5(t1, {full_name: "Norbert Weishaupt", age: "65"});
task6(t1, "country", "Australia");