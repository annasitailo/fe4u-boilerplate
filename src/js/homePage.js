const openAddModals = document.querySelectorAll('#addOpenButton')
const openFCardModals = document.querySelectorAll('#open-teach-f-card')
const closeModalButtons = document.querySelectorAll('[data-close-button]')
const closeFCardButtons = document.querySelectorAll('#close-teach-f-card')
const overlay = document.getElementById('overlay')

openAddModals.forEach(button => {
    button.addEventListener('click', () => {
        const modal = document.getElementById('modalAdd')
        openModal(modal)
    })
})

closeModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest('.modal')
        closeModal(modal)
    })
})

openFCardModals.forEach(button => {
    button.addEventListener('click', () => {
        const modal = document.getElementById('teach-f-card-modal')
        openModal(modal)
    })
})

closeFCardButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest('.teach-f-card-modal')
        closeModal(modal)
    })
})

function openModal(modal) {
    if (modal == null) return
    modal.classList.add('active')
    overlay.classList.add('active')
}

function closeModal(modal) {
    if (modal == null) return
    modal.classList.remove('active')
    overlay.classList.remove('active')
}
